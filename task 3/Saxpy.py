def saxpy(a, vector_x, vector_y):
    vector_z = [x_ * a + y_ for x_, y_ in zip(vector_x, vector_y)]
    return vector_z
